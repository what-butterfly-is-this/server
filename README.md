# What Butterfly Is This: Server

REST API written in Flask. Exposes a POST endpoint to receive images and classify them.

## Usage

run the following command to install dependencies

```
pip install -r requirements.txt
```

start the server

```
python main.py
```
