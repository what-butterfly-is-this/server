import base64
import io
import pickle
import random

import torch
from flask import Flask, request
from PIL import Image
from torch import nn
from torchvision import models, transforms

from info import ButterflyInfo, info

with open("id2label", "rb") as f:
    id2label = pickle.load(f)

model = models.resnet18()
model.fc = nn.Linear(model.fc.in_features, 101)
state_dict = torch.load("model_state_dict.pth", map_location="cpu")
model.load_state_dict(state_dict)
model.eval()


def get_image_transforms(size):
    return transforms.Compose(
        [
            transforms.CenterCrop((size, size)),
            transforms.Resize(
                (224, 224), interpolation=transforms.InterpolationMode.BICUBIC
            ),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
            ),
        ]
    )


app = Flask(__name__)


@app.route("/", methods=["POST"])
def classify():
    # read image contents from HTTP request
    image_data = request.form["image"]
    # decode from base64 to binary
    image_bytes = base64.b64decode(image_data)
    # convert to PIL image
    image = Image.open(io.BytesIO(image_bytes))
    # find the shortest image dimension to crop it square
    shorter_side = min(image.width, image.height)
    # apply preprocessing before running inference
    image = get_image_transforms(shorter_side)(image)
    # classify the image with network
    logits = model(image.unsqueeze(0))
    confidence = nn.functional.softmax(logits, dim=1).max().item()
    # get the id of the class the model gives the highest probability to
    predicted_class_id: int = logits.argmax().item()
    # convert the class id into the butterfly species name
    predicted_class_label: str = id2label[predicted_class_id]
    if predicted_class_label == "OTHER":
        predicted_class_info: ButterflyInfo = {"url": "", "desc": ""}
        random_image_url = ""
    else:
        predicted_class_info: ButterflyInfo = info[predicted_class_label]
        # select a random image for this class
        random_image_url: str = f"https://gitlab.com/what-butterfly-is-this/server/-/raw/main/images/{predicted_class_label.replace(' ', '%20')}/{random.randint(1, 5)}.jpg"
    # respond to the HTTP request with the predicted species
    return {
        "label": predicted_class_label,
        "id": predicted_class_id,
        "image": random_image_url,
        "wiki": predicted_class_info["url"],
        "description": predicted_class_info["desc"],
        "confidence": confidence,
    }


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
